package com.yousuf.moviedbdemo.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.yousuf.moviedbdemo.R;
import com.yousuf.moviedbdemo.model.MovieItem;

public class MovieViewHolder extends RecyclerView.ViewHolder{

    ImageView imageView;
    TextView textViewTitle;
    TextView textViewYear;

    public MovieViewHolder(View itemView) {
        super(itemView);

        imageView = itemView.findViewById(R.id.imageView);
        textViewTitle = itemView.findViewById(R.id.textViewTitle);
        textViewYear = itemView.findViewById(R.id.textViewSubTitle);
    }




    public void bind(MovieItem movieItem){

        Glide.with(imageView.getContext())
                .load("http://image.tmdb.org/t/p/w185/" + movieItem.posterPath)
                .into(imageView);

        textViewTitle.setText(movieItem.getTitle());
        textViewYear.setText(movieItem.getReleaseDate());

    }
}
