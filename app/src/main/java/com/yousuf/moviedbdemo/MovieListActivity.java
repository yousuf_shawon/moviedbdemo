package com.yousuf.moviedbdemo;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.yousuf.moviedbdemo.fragments.LoadingFragment;
import com.yousuf.moviedbdemo.fragments.MovieListFragment;
import com.yousuf.moviedbdemo.util.Constants;
import com.yousuf.moviedbdemo.viewmodels.MovieListViewModel;

public class MovieListActivity extends AppCompatActivity {

    private MovieListViewModel movieListViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_list);

        movieListViewModel = ViewModelProviders.of(this).get(MovieListViewModel.class);

//        if (savedInstanceState == null) {
//            getSupportFragmentManager().beginTransaction()
//                    .replace(android.R.id.content,  LoadingFragment.newInstance(), LoadingFragment.class.getName())
//                    .commit();
//        }


        movieListViewModel.getActivityState().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer integer) {

                if (integer== Constants.LOADING_STATE) {

                    updateFragment(LoadingFragment.newInstance());

                }else if(integer == Constants.LIST_STATE){

                    updateFragment(MovieListFragment.newInstance());

                }else if(integer == Constants.SEARCH_STATE){

                }

            }
        });


        movieListViewModel.setActivityState(Constants.LOADING_STATE);


    }




    public void updateFragment(@NonNull Fragment fragment){

        String tag = fragment.getClass().getSimpleName();

        getSupportFragmentManager()
                .beginTransaction()
                //.addToBackStack(tag)
                .replace(R.id.content, fragment, tag)
                .commit();

    }

}
