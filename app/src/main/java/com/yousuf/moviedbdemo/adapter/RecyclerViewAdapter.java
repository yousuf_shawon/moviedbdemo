package com.yousuf.moviedbdemo.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.yousuf.moviedbdemo.R;
import com.yousuf.moviedbdemo.model.MovieItem;
import com.yousuf.moviedbdemo.model.MovieResponse;
import com.yousuf.moviedbdemo.util.Constants;
import com.yousuf.moviedbdemo.viewholder.MovieViewHolder;

import java.util.List;

import timber.log.Timber;

public class RecyclerViewAdapter extends RecyclerView.Adapter<MovieViewHolder> {

    private int listType;
    private MovieResponse movieResponse;
    List<MovieItem> movieItemList;
    private int page;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;

        Timber.i("PAGE: List Type: " + listType + " page: " + this.page );
    }

    public void swapData(List<MovieItem> movieItems) {

        if (movieItems != null) {

            if (movieItemList.size() < movieItems.size()) {
                setPage(getPage()+1);
            }

            movieItemList.clear();
            movieItemList.addAll(movieItems);
            notifyDataSetChanged();

        }

    }

    public interface ListUpdateListener{

        public void fetchNewData(int listType, int page);

    }

    ListUpdateListener mListener;


    public RecyclerViewAdapter(int listType , List<MovieItem> itemList) {
        this.listType = listType;
       // this.movieResponse = movieResponse;
        this.movieItemList = itemList;
        this.page = 0;
    }

    @NonNull
    @Override
    public MovieViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.album_card, parent, false);

        MovieViewHolder movieViewHolder = new MovieViewHolder(view);

        return movieViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MovieViewHolder holder, int position) {

        holder.bind(movieItemList.get(position));

        if (position + 1 == movieItemList.size()){

            Timber.i("PAGE:  Position: " + position +  " List Type: " + listType + " page: " + this.page );

            mListener.fetchNewData(listType, page+1);

//            if (listType== Constants.TOP_LIST) {
//
//                if (mListener != null) {
//                    mListener.
//                }
//
//            }else if( listType == Constants.NOW_PLAYING_LIST ){
//
//            }else if( listType == Constants.UP_COMING_LIST ){
//
//            }

        }


    }

    @Override
    public int getItemCount() {
        return movieItemList.size();
    }


    public void setmListener(ListUpdateListener mListener) {
        this.mListener = mListener;
    }
}
