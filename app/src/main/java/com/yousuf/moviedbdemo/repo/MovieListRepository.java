package com.yousuf.moviedbdemo.repo;

import android.arch.lifecycle.MutableLiveData;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.yousuf.moviedbdemo.AppApplication;
import com.yousuf.moviedbdemo.model.MovieItem;
import com.yousuf.moviedbdemo.model.MovieResponse;
import com.yousuf.moviedbdemo.rest.ApiClient;
import com.yousuf.moviedbdemo.rest.ApiService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import timber.log.Timber;

public class MovieListRepository {


    MutableLiveData<List<MovieItem>> topRatedMovieList;

    MutableLiveData<List<MovieItem>> nowPlayingMovieList;

    MutableLiveData<List<MovieItem>> upComingMovieList;

    Retrofit apiClient;
    private  ApiService apiService;

    public MovieListRepository() {

        topRatedMovieList = new MutableLiveData<>();
        nowPlayingMovieList = new MutableLiveData<>();
        upComingMovieList = new MutableLiveData<>();

        apiClient = ApiClient.getClient();
        apiService = apiClient.create(ApiService.class);

        loadMovies();

    }


    public void loadMovies() {


       loadTopRatedMovies(1);
       loadNowPlayingMovies(1);
       loadUpComingMovieList(1);

    }

    public void loadUpComingMovieList(int page) {

        Timber.i("PAGE: Up Coming  Loading for: " + page);

        Call<MovieResponse> call = apiService.getUpcomingMovies(page);

        call.enqueue(new Callback<MovieResponse>() {
            @Override
            public void onResponse(Call<MovieResponse> call, Response<MovieResponse> response) {
                if (response.isSuccessful()) {
                    Timber.i("Upcoming Total Size " + response.body().getMovieItemList().size());

                    List<MovieItem> itemList = upComingMovieList.getValue();

                    if ( itemList != null && itemList.size() > 0) {
                        itemList.addAll(response.body().getMovieItemList());
                        upComingMovieList.setValue(itemList);
                    }else{
                        upComingMovieList.setValue(response.body().getMovieItemList());
                    }


                }else {
                    upComingMovieList.setValue(null);
                    Timber.i("Upcoming Request Unsuccessful ");
                }

            }

            @Override
            public void onFailure(Call<MovieResponse> call, Throwable t) {
                upComingMovieList.setValue(null);
                Timber.i("Upcoming Request Failed ");
                Toast.makeText(AppApplication.getInstance().getApplicationContext(), "Check Internet Connection", Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void loadNowPlayingMovies(int page) {

        Timber.i("PAGE: Now Playing  Loading for: " + page);

        Call<MovieResponse> call = apiService.getNowPlayingMovies(page);

        call.enqueue(new Callback<MovieResponse>() {
            @Override
            public void onResponse(Call<MovieResponse> call, Response<MovieResponse> response) {

                if (response.isSuccessful()) {
                    Timber.i("Now Playing Total Size: " + response.body().getMovieItemList().size());

                    List<MovieItem> itemList = nowPlayingMovieList.getValue();

                    if ( itemList != null && itemList.size() > 0) {
                        itemList.addAll(response.body().getMovieItemList());
                        nowPlayingMovieList.setValue(itemList);
                    }else{
                        nowPlayingMovieList.setValue(response.body().getMovieItemList());
                    }


                }else {
                    nowPlayingMovieList.setValue(null);
                    Timber.i("Now Playing Request Unsuccessful ");
                }

            }

            @Override
            public void onFailure(Call<MovieResponse> call, Throwable t) {
                nowPlayingMovieList.setValue(null);
                Timber.i("Now Playing Request Failed ");
                Toast.makeText(AppApplication.getInstance().getApplicationContext(), "Check Internet Connection", Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void loadTopRatedMovies(int page) {

        Timber.i("PAGE: Top Rated  Loading for: " + page);

        Call<MovieResponse> call = apiService.getTopRatedMovies(page);

        call.enqueue(new Callback<MovieResponse>() {
            @Override
            public void onResponse(Call<MovieResponse> call, Response<MovieResponse> response) {
                if (response.isSuccessful()) {
                    Timber.i("Top Rated Total Size: " + response.body().getMovieItemList().size());
                    List<MovieItem> itemList = topRatedMovieList.getValue();

                    if ( itemList != null && itemList.size() > 0) {
                        itemList.addAll(response.body().getMovieItemList());
                        topRatedMovieList.setValue(itemList);
                    }else{
                        topRatedMovieList.setValue(response.body().getMovieItemList());
                    }


                }else {
                    topRatedMovieList.setValue(null);
                    Timber.i("TopRated Request Unsuccessful ");
                }

            }

            @Override
            public void onFailure(Call<MovieResponse> call, Throwable t) {
                topRatedMovieList.setValue(null);
                Timber.i("TopRated Request Failed ");
                Toast.makeText(AppApplication.getInstance().getApplicationContext(), "Check Internet Connection", Toast.LENGTH_SHORT).show();
            }
        });
    }


    public MutableLiveData<List<MovieItem>> getTopRatedMovieList() {
        return topRatedMovieList;
    }

    public MutableLiveData<List<MovieItem>> getNowPlayingMovieList() {
        return nowPlayingMovieList;
    }

    public MutableLiveData<List<MovieItem>> getUpComingMovieList() {
        return upComingMovieList;
    }
}
