package com.yousuf.moviedbdemo.fragments;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.yousuf.moviedbdemo.R;
import com.yousuf.moviedbdemo.util.Constants;
import com.yousuf.moviedbdemo.viewmodels.MovieListViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoadingFragment extends Fragment {


    private MovieListViewModel movieListViewModel;


    public LoadingFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        movieListViewModel = ViewModelProviders.of(getActivity()).get(MovieListViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_loading, container, false);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

//        movieListViewModel.getActivityState().observe(this, new Observer<Integer>() {
//            @Override
//            public void onChanged(@Nullable Integer integer) {
//                movieListViewModel.setActivityState(Constants.LOADING_STATE);
//            }
//        });


        waitForSomeTime();

    }

    private void waitForSomeTime() {

        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {

                movieListViewModel.setActivityState(Constants.LIST_STATE);
            }
        }, 2000);

    }

    public static LoadingFragment newInstance() {
        
        Bundle args = new Bundle();
        
        LoadingFragment fragment = new LoadingFragment();
        fragment.setArguments(args);
        return fragment;
    }

}
