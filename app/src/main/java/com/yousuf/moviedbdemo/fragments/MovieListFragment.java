package com.yousuf.moviedbdemo.fragments;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.yousuf.moviedbdemo.R;
import com.yousuf.moviedbdemo.adapter.RecyclerViewAdapter;
import com.yousuf.moviedbdemo.model.MovieItem;
import com.yousuf.moviedbdemo.model.MovieResponse;
import com.yousuf.moviedbdemo.util.Constants;
import com.yousuf.moviedbdemo.viewmodels.MovieListViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class MovieListFragment extends Fragment {


    private MovieListViewModel movieListViewModel;

    RecyclerView recyclerViewTop, recyclerViewPlaying, recyclerViewUpComing;

    RecyclerViewAdapter adapterTop, adapterNowPlaying, adapterUpComing;

    ProgressBar progressBar1, progressBar2, progressBar3;

    List<MovieItem> movieItemListTop, movieItemListNowPlaying, movieItemListUpComing;




    public MovieListFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        movieListViewModel = ViewModelProviders.of(getActivity()).get(MovieListViewModel.class);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
         View view =  inflater.inflate(R.layout.fragment_movie_list, container, false);

        initializeViews( view);

         return view;
    }

    private void initializeViews(View view) {

        View view1 = view.findViewById(R.id.layout_top);
        View view2 = view.findViewById(R.id.layout_now);
        View view3 = view.findViewById(R.id.layout_upcoming);

        TextView textView1 = view1.findViewById(R.id.textViewHeader);
        TextView textView2 = view2.findViewById(R.id.textViewHeader);
        TextView textView3 = view3.findViewById(R.id.textViewHeader);

        textView1.setText("Top Rated");
        textView2.setText("Now Playing");
        textView3.setText("Up Coming");

        recyclerViewTop = view1.findViewById(R.id.recyclerViewList);
        recyclerViewPlaying = view2.findViewById(R.id.recyclerViewList);
        recyclerViewUpComing = view3.findViewById(R.id.recyclerViewList);

        progressBar1 = view1.findViewById(R.id.progressBar);
        progressBar2 = view2.findViewById(R.id.progressBar);
        progressBar3 = view3.findViewById(R.id.progressBar);


        movieItemListTop = new ArrayList<>();
        movieItemListNowPlaying = new ArrayList<>();
        movieItemListUpComing = new ArrayList<>();


        MovieResponse movieResponse1 = new MovieResponse();
        movieResponse1.setMovieItemList(movieItemListTop);


        MovieResponse movieResponse2 = new MovieResponse();
        movieResponse2.setMovieItemList(movieItemListNowPlaying);


        MovieResponse movieResponse3 = new MovieResponse();
        movieResponse3.setMovieItemList(movieItemListUpComing);


        adapterTop = new RecyclerViewAdapter(Constants.TOP_LIST, movieItemListTop);
        adapterNowPlaying = new RecyclerViewAdapter(Constants.NOW_PLAYING_LIST,  movieItemListNowPlaying);
        adapterUpComing = new RecyclerViewAdapter(Constants.UP_COMING_LIST, movieItemListUpComing);

        adapterTop.setmListener(new RecyclerViewAdapter.ListUpdateListener() {
            @Override
            public void fetchNewData(int listType, int page) {

               progressBar1.setVisibility(View.VISIBLE);
                movieListViewModel.loadTopRatedMore(page);
            }
        });


        adapterNowPlaying.setmListener(new RecyclerViewAdapter.ListUpdateListener() {
            @Override
            public void fetchNewData(int listType, int page) {
                progressBar2.setVisibility(View.VISIBLE);
                movieListViewModel.loadNowPlayingMore(page);
            }
        });


        adapterUpComing.setmListener(new RecyclerViewAdapter.ListUpdateListener() {
            @Override
            public void fetchNewData(int listType, int page) {
                progressBar3.setVisibility(View.VISIBLE);
                movieListViewModel.loadUpComingMore(page);
            }
        });


        recyclerViewTop.setAdapter(adapterTop);
        recyclerViewPlaying.setAdapter(adapterNowPlaying);
        recyclerViewUpComing.setAdapter(adapterUpComing);


        recyclerViewTop.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        recyclerViewTop.setAdapter(adapterTop);
      //  recyclerViewTop.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.HORIZONTAL));


        recyclerViewPlaying.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        recyclerViewPlaying.setAdapter(adapterNowPlaying);
      //  recyclerViewPlaying.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.HORIZONTAL));

        recyclerViewUpComing.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        recyclerViewUpComing.setAdapter(adapterUpComing);
      //  recyclerViewUpComing.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.HORIZONTAL));







    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);



        getMovieList();


    }

    private void getMovieList() {

        getTopRatedMovie(1);
        getNowPlayingMovie(1);
        getUpComingMovie(1);


    }

    private void getTopRatedMovie(int page) {

        movieListViewModel.getTopRatedMoviesByPage().observe(this, new Observer<List<MovieItem>>() {
            @Override
            public void onChanged(@Nullable List<MovieItem> movieItems) {

                progressBar1.setVisibility(View.GONE);

                if (movieItems != null) {

                    int preSize = adapterTop.getItemCount();
                    adapterTop.swapData(movieItems);

                    recyclerViewTop.getLayoutManager().scrollToPosition(preSize);

                }
            }
        });

    }

    private void getNowPlayingMovie(int page) {

        movieListViewModel.getNowPlayingMoviesByPage().observe(this, new Observer<List<MovieItem>>() {
            @Override
            public void onChanged(@Nullable List<MovieItem> movieItems) {
                progressBar2.setVisibility(View.GONE);
                if (movieItems != null) {

                    int preSize = adapterNowPlaying.getItemCount();

                    adapterNowPlaying.swapData(movieItems);
                    recyclerViewPlaying.getLayoutManager().scrollToPosition(preSize);



                }

            }
        });

    }

    private void getUpComingMovie(int page) {

        movieListViewModel.getUpComingMoviesByPage().observe(this, new Observer<List<MovieItem>>() {
            @Override
            public void onChanged(@Nullable List<MovieItem> movieItems) {

                progressBar3.setVisibility(View.GONE);
                if (movieItems != null) {

                    int preSize = adapterUpComing.getItemCount();
                    adapterUpComing.swapData(movieItems);
                    recyclerViewUpComing.getLayoutManager().scrollToPosition(preSize);

                }

            }
        });

    }




    public static MovieListFragment newInstance() {
        
        Bundle args = new Bundle();
        
        MovieListFragment fragment = new MovieListFragment();
        fragment.setArguments(args);
        return fragment;
    }

}
