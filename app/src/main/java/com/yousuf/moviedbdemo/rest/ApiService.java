package com.yousuf.moviedbdemo.rest;

import com.yousuf.moviedbdemo.model.MovieItem;
import com.yousuf.moviedbdemo.model.MovieResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiService {



    @GET("top_rated")
    Call<MovieResponse> getTopRatedMovies(@Query("page") int page);

    @GET("now_playing")
    Call<MovieResponse> getNowPlayingMovies(@Query("page") int page);

    @GET("upcoming")
    Call<MovieResponse> getUpcomingMovies(@Query("page") int page);

}
