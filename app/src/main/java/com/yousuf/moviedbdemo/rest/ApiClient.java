package com.yousuf.moviedbdemo.rest;

import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;

import com.yousuf.moviedbdemo.AppApplication;


import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import timber.log.Timber;

public class ApiClient {

    public static final String BASE_URL = "https://api.themoviedb.org/3/movie/";

    private static final String API_KEY = "api_key";
    private static final String LANGUAGE_KEY = "language";
    private static Retrofit retrofit = null;

    private static final String CACHE_CONTROL = "Cache-Control";


    private static String TAG = ApiClient.class.getSimpleName();

    private static OkHttpClient okHttpClient;
    private static String API_KEY_VALUE = "4bc53930f5725a4838bf943d02af6aa9";
    private static final String LANGUAGE_KEY_VALUE = "en-US";


    public static Retrofit getClient() {

        if (retrofit==null) {

            okHttpClient = getOkHttpClient();

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }




    private static OkHttpClient getOkHttpClient(){

        return new OkHttpClient.Builder()
                .addInterceptor(provideUrlInterceptor())
                .addInterceptor(provideLoginInterceptor())
                .addInterceptor(provideOfflineCacheInterceptor())
                .addNetworkInterceptor(provideCacheInterceptor())
                .cache(provideCache())
                .build();
    }

    private static Interceptor provideUrlInterceptor() {
        return new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {

                Request request = chain.request();
                Request.Builder builder = request.newBuilder();

                HttpUrl url = request.url().newBuilder()
                        .addQueryParameter(API_KEY, API_KEY_VALUE)
                        .addQueryParameter(LANGUAGE_KEY,LANGUAGE_KEY_VALUE )
                        .build();

                builder.url(url);

                return chain.proceed(builder.build());
            }
        };
    }

    private static Interceptor provideLoginInterceptor(){

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        return loggingInterceptor;
    }



    private static Cache provideCache(){
        Cache cache = null;


        try {

            cache = new Cache(new File(AppApplication.getInstance().getCacheDir(), "http-cache"),
                    10* 1024 * 1024);  // 10 MB
        }catch (Exception e){
            Timber.e( e.getMessage());
        }

        return cache;
    }



    private static Interceptor provideCacheInterceptor(){
        return new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {

                Response response = chain.proceed(chain.request());

                CacheControl cacheControl = new CacheControl.Builder()
                        .maxAge(2, TimeUnit.MINUTES)
                        .build();

                return response.newBuilder()
                        .header(CACHE_CONTROL, cacheControl.toString())
                        .build();
            }
        };
    }


    private static Interceptor provideOfflineCacheInterceptor(){

        return new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {

                Request request = chain.request();

                if (!AppApplication.hasNetwork()) {

                    CacheControl cacheControl = new CacheControl.Builder()
                            .maxStale(7, TimeUnit.DAYS)
                            .build();

                    request = request.newBuilder()
                            .cacheControl(cacheControl)
                            .build();

                }

                return chain.proceed(request);

            }
        };

    }

}
