package com.yousuf.moviedbdemo.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MovieResponse {

    public int page;

    @SerializedName("total_results")
    public int totalResult;

    @SerializedName("total_pages")
    public int totalPage;

    @SerializedName("results")
    public List<MovieItem> movieItemList;


    public int getPage() {
        return page;
    }

    public int getTotalResult() {
        return totalResult;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public List<MovieItem> getMovieItemList() {
        return movieItemList;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public void setTotalResult(int totalResult) {
        this.totalResult = totalResult;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public void setMovieItemList(List<MovieItem> movieItemList) {
        this.movieItemList = movieItemList;
    }

    @Override
    public String toString() {
        return "MovieResponse{" +
                "page=" + page +
                ", totalResult=" + totalResult +
                ", totalPage=" + totalPage +
                ", movieItemList=" + movieItemList == null ? "null " : ("Total" + movieItemList.size() )+
                '}';
    }
}
