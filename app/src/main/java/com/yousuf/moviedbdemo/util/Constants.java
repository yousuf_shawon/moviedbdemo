package com.yousuf.moviedbdemo.util;

public class Constants {


    public Constants() {
        new AssertionError("This class can't be initialized");
    }


    public static int LOADING_STATE  =1;
    public static int LIST_STATE  =2;
    public static int SEARCH_STATE  =3;

    public static int TOP_LIST = 10;
    public static int NOW_PLAYING_LIST = 11;
    public static int UP_COMING_LIST = 12;


}
