package com.yousuf.moviedbdemo.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.yousuf.moviedbdemo.model.MovieItem;
import com.yousuf.moviedbdemo.repo.MovieListRepository;

import java.util.List;

public class MovieListViewModel extends AndroidViewModel {


    private MutableLiveData<Integer> activityState;

    private MovieListRepository movieListRepository;





    public MovieListViewModel(@NonNull Application application) {
        super(application);

        activityState = new MutableLiveData<>();
        movieListRepository = new MovieListRepository();

       // movieListRepository.loadMovies();

    }

    public void setActivityState(int loadingState) {

        activityState.setValue(loadingState);

    }

    public LiveData<Integer> getActivityState() {
        return activityState;
    }

    public LiveData<List<MovieItem>> getTopRatedMoviesByPage() {

        return movieListRepository.getTopRatedMovieList();

    }

    public LiveData<List<MovieItem>> getNowPlayingMoviesByPage() {

        return movieListRepository.getNowPlayingMovieList();

    }

    public LiveData<List<MovieItem>> getUpComingMoviesByPage() {

        return movieListRepository.getUpComingMovieList();

    }


    public void loadTopRatedMore(int page){
        movieListRepository.loadTopRatedMovies(page);
    }

    public void loadNowPlayingMore(int page){
        movieListRepository.loadNowPlayingMovies(page);
    }

    public void loadUpComingMore(int page){

        movieListRepository.loadUpComingMovieList(page);

    }


}
